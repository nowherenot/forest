//TODO
//Remove double rocks

//Crafty.dontPauseOnBlur=true;
var Forest = {
    //Generate with
    // var a=[];
    // for (key in Crafty.assets) a.push(key);
    // a;
    assets: ["loading.png", "fox.png", "mouse.png", "rabe.png", "fire.png", "bear.png", "hase.png", "star.png", "mushroom.png"],
    mouseTriggered: false,
    mouseMet: false,
    screenWidth: 808,
    screenHeight: 442,
    speed: 16,
    jump: 8,
    animalsPlaying: 0,
    disabledNotes: {},
    totalWidth: 8 * 800,
    init: function() {
        var start = function(bg, loadingtext) {
            bg.destroy();
            loadingtext.destroy();
            //Crafty.audio.play("ambient");
            Crafty.scene('main');
        };
        //Setup stuff
        Crafty.scene("loading", function() {
            // Add assets
            for (var i = 0; i < 8; i++) {
                F.assets.push("back" + i + ".png");
                F.assets.push("fore" + i + ".png");
            }
            // Background image with some loading text
            var bg = Crafty.e("2D, Canvas, Image, Mouse")
                .attr({
                    w: Crafty.viewport.width,
                    h: Crafty.viewport.height,
                    z: 50
                })
                .image("loading.png");
            var loadingtext = Crafty.e("2D, DOM, Text, Keyboard");
            loadingtext.attr({
                w: Crafty.viewport.width,
                x: 0,
                y: 120,
                z: 50
            });
            loadingtext.text("<h1>Forest</h1><h3>Loading</h3>");
            loadingtext.textFont({
                size: "20px",
                family: "Silkscreen"
            });
            loadingtext.css({
                "text-align": "center",
                "font-family": "Silkscreen"
            });

            // Load takes an array of assets and a callback when complete

            Crafty.load(Forest.assets, function() {
                loadingtext.text("<h1>Forest</h1><h3>Press Enter to start</h3><br><br><br>")
                    .bind("KeyDown", function(e) {
                        if (this.isDown("ENTER") || this.isDown("SPACE")) {
                            start(bg, loadingtext);
                        }
                    });
                //Touch
                bg.bind("Click", function() {
                    start(bg, loadingtext);
                });
            });

        });

        Crafty.scene("main", function() {
            Crafty.audio.add("ambient", ["ambient.mp3", "ambient.ogg"]);
            Crafty.audio.add("music", ["score2.mp3", "score2.ogg"]);

            Crafty.audio.play("ambient", 100);

            //Set up Images
            //Create background and foreground
            F.backgrounds = [];
            F.foregrounds = [];
            for (var i = 0; i < 8; i++) {
                F.backgrounds.push(Crafty.e("2D, Canvas, Image").image("back" + i + ".png"));
                F.backgrounds[i].x = i * 800;
                F.backgrounds[i].z = -5;
                F.foregrounds.push(Crafty.e("2D, Canvas, Image").image("fore" + i + ".png"));
                F.foregrounds[i].x = i * 800;
                F.foregrounds[i].z = 20;
            }
            // F.foreground = Crafty.e("2D, Canvas, Mouse")
            //     .attr({
            //         w: 4724,
            //         h: Crafty.viewport.height,
            //     });

            // F.foreground.bind('MouseDown', function(e) {
            //     //Left
            //     if (e.clientX < Crafty.stage.x + 50) {
            //         Crafty.keydown[37] = true;
            //     }
            //     //Right
            //     if (e.clientX > Crafty.stage.x + Crafty.viewport.width - 50) {
            //         Crafty.keydown[39] = true;
            //     }
            // });
            // F.foreground.bind('Click', function(e) {
            //     Crafty.keydown = {};
            // });

            Crafty.sprite(190, "fox.png", {
                foxSprite: [0, 0]
            });
            Crafty.sprite(102, "mouse.png", {
                mouseSprite: [0, 0]
            });
            Crafty.sprite(120, "rabe.png", {
                crowSprite: [0, 0]
            });
            Crafty.sprite(74, "fire.png", {
                fireSprite: [0, 0]
            });
            Crafty.sprite(250, "bear.png", {
                bearSprite: [0, 0]
            });
            Crafty.sprite(215, "hase.png", {
                rabbitSprite: [0, 0]
            });
            Crafty.sprite(100, "squirrel.png", {
                squirrelSprite: [0, 0]
            });
            Crafty.sprite(40, "mute.png", {
                muteSprite: [0, 0]
            });

            F.floor = Crafty.e("2D, Canvas, floor").attr({
                x: 0,
                y: 415,
                w: 8454,
                h: 5
            });
            F.rocks = Crafty.e("2D, Canvas, floor, Collision").attr({
                x: 5600,
                y: 400,
                w: 430,
                h: 50
            });
            F.rocks2 = Crafty.e("2D, Canvas, floor, Collision").attr({
                x: 5700,
                y: 360,
                w: 260,
                h: 80
            });
            //css({border: '1px solid red'})

            F.player = F.playerFac();

            //Floors
            F.player.bind('EnterFrame', function() {
                if (this.within({
                    x: 4880,
                    y: 220,
                    w: 370,
                    h: 250
                })) {
                    F.floor.y = 440;
                } else if (this.x > 3500) {
                    if (this.y + this.h > 425) this.y = (425 - 1) - this.h;
                    F.floor.y = 425;
                } else {
                    if (this.y + this.h > 415) this.y = (415 - 1) - this.h;
                    F.floor.y = 415;
                }
            });

            var poly = new Crafty.polygon([21, 186], [24, 40], [201, 40], [188, 183]);
            F.player.collision(poly);

            //Create event zones
            var mouseEventZone = Crafty.e("2D, Canvas, Collision, mouseEventZone")
                .attr({
                    x: 2000,
                    y: 0,
                    w: 15,
                    h: 600
                })
                .collision();
            var crowEventZone = Crafty.e("2D, Canvas, Collision, crowEventZone")
                .attr({
                    x: 3065,
                    y: 0,
                    w: 30,
                    h: 600
                })
                .collision();

            F.dustZone = Crafty.e("2D, Canvas, Particles")
                .attr({
                    x: 2065,
                    y: 0,
                    w: 300,
                    h: 150
                })
                .particles({
                    maxParticles: 100,
                    size: 2,
                    sizeRandom: 2,
                    speed: 0.01,
                    speedRandom: 0.05,
                    lifeSpan: 141,
                    lifeSpanRandom: 7,
                    angle: 180,
                    angleRandom: 180,
                    startColour: [255, 131, 10, 0.9],
                    startColourRandom: [25, 25, 25, 0.4],
                    endColour: [245, 85, 20, 0.6],
                    endColourRandom: [25, 25, 25, 0.2],
                    sharpness: 20,
                    sharpnessRandom: 10,
                    spread: 300,
                    duration: -1,
                    fastMode: true,
                    gravity: {
                        x: 0,
                        y: 0.001
                    }
                });

            // Launch the animations, changing sprite every 20ms
            F.crow = Crafty.e("2D, Canvas, crowSprite, SpriteAnimation, crow")
                .attr({
                    x: 3080,
                    y: 19
                })
                .reel("crowWithCheese", 1000, 3, 0, 4)
                .reel("crowFlying", 1000, 0, 0, 3)
                .animate("crowWithCheese", -1);
            //150

            F.bear = Crafty.e("2D, Canvas, bearSprite, SpriteAnimation, bear")
                .attr({
                    x: 4080 + 500,
                    y: 170
                })
                .reel("bearPlaying", 800, 0, 0, 4)
                .reel("bearTired", 800, 4, 0, 2)
                .animate("bearTired", -1);

            F.rabbit = Crafty.e("2D, Canvas, rabbitSprite, SpriteAnimation, rabbit")
                .attr({
                    x: 3850 + 500,
                    y: 210
                })
                .reel("rabbitPlaying", 800, 0, 0, 4)
                .reel("rabbitTired", 800, 4, 0, 2)
                .animate("rabbitTired", -1);
            //50

            F.squirrel = Crafty.e("2D, Canvas, squirrelSprite, SpriteAnimation, squirrel")
                .attr({
                    x: 6200,
                    y: 320
                })
                .reel("squirrel", 800, 0, 0, 2)
                .animate("squirrel", -1);
            //80

            F.fire = Crafty.e("2D, Canvas, fireSprite, SpriteAnimation, fire, Particles")
                .attr({
                    x: 4010 + 500,
                    y: 344
                })
                .reel("fire", 0, 0, 2)
                .reel("fire", 40, -1)
                .particles({
                    canvasWidth: F.screenWidth,
                    canvasHeight: F.screenHeight,
                    maxParticles: 100,
                    size: 1,
                    sizeRandom: 3,
                    speed: 2.5,
                    speedRandom: 2,
                    lifeSpan: 341,
                    lifeSpanRandom: 100,
                    angle: 0,
                    angleRandom: 14,
                    startColour: [255, 131, 10, 0.9],
                    startColourRandom: [25, 25, 25, 0.4],
                    endColour: [205, 85, 40, 0.1],
                    endColourRandom: [25, 25, 25, 0.2],
                    sharpness: 50,
                    sharpnessRandom: 10,
                    spread: 50,
                    duration: -1,
                    fastMode: true,
                    gravity: {
                        x: 0,
                        y: 0.009
                    },
                    jitter: 2
                });

            F.mushroom = Crafty.e("2D, Canvas, Image, Collision, mushroom")
                .attr({
                    x: 3600,
                    y: 350
                })
                .image('mushroom.png');

            setTimeout(function() {
                F.createStars();
            }, 40);

            F.player.onHit("mouseEventZone", function() {
                F.showMouse();
                mouseEventZone.destroy();
            });
            F.showNote('Talk to crow', "crowEventZone", function() {
                F.dialogCrow();
            }, 2980);

            F.showNote('Pick mushroom', "mushroom", function() {
                F.dialogMushroom();
            }, 3450);

            F.showNote('Talk to jackrabbit', "rabbit", function() {
                F.dialogRabbit();
            }, 4250);
            F.showNote('Talk to bear', "bear", function() {
                F.dialogBear();
            }, 4480);
            F.showNote('Talk to squirrel', "squirrel", function() {
                F.dialogSquirrel();
            }, 5970);

            var mute = false;
            F.muteButton = Crafty.e("2D, Canvas, SpriteAnimation, Mouse, muteSprite").attr({
                x: 10,
                y: 10,
                z: 20
            }).bind('Click', function() {
                if (mute) {
                    Crafty.audio.unmute();
                    this.sprite(0, 0, 1, 1);
                    mute = false;
                    Crafty.bind("Unpause", function() {
                        Crafty.audio.unmute();
                    });
                } else {
                    Crafty.audio.mute();
                    this.sprite(1, 0, 1, 1);
                    mute = true;

                    Crafty.unbind('Unpause');
                }
            }).bind('EnterFrame', function() {
                //this.x = -Crafty.viewport.x;

            }).sprite(0, 0, 1, 1);
        });

        //start crafty
        Crafty.init(808, 522);
        //Crafty.pixelart(true);

        //Load image
        Crafty.load(["loading.png"], function() {
            Crafty.scene("loading");
        });
    },
    inventory: {},
    playerFac: function() {
        var nearToRock = function(a) {
            return (a.x + a.w >= F.rocks.x && a.x <= F.rocks.x + F.rocks.w && a.y >= 225) || (a.x + a.w >= F.rocks2.x && a.x <= F.rocks2.x + F.rocks2.w && a.y >= 210);
        };
        var wasLast;
        return Crafty.e("2D, Canvas, foxSprite, Twoway, SpriteAnimation, Collision, Gravity, Player, Keyboard")
            .attr({
                x: 0,
                y: 344,
                z: 1
            })
            .gravity("floor")
            .twoway(F.speed, F.jump)
            .reel("walk_right", 500, 0, 0, 4)
            .reel("walk_left", 500, 4, 0, 4)
            .bind("KeyUp", function(e) {
                this.pauseAnimation();
            })
            .bind("EnterFrame", this.animationFn = function(e) {
                if (this.disableControls) return;
                this.relativeX = F.player.x + Crafty.viewport.x;
                if (this.isDown("LEFT_ARROW") || this.isDown("A")) {
                    if (this.relativeX < 50 && Crafty.viewport.x < 0) {
                        Crafty.viewport.x += this._speed;
                    }
                    if (this.x <= 0 || nearToRock(this)) {
                        this.x += this._speed;
                    }
                    if (!this.isPlaying("walk_left")) {
                        this.animate("walk_left");
                    }
                    wasLast = 'left';
                } else if (this.isDown("RIGHT_ARROW") || this.isDown("D")) {
                    if (this.relativeX > 450 && Crafty.viewport.x > -(F.totalWidth - Crafty.viewport.width) + 24) {
                        Crafty.viewport.x -= this._speed;
                    }
                    if (this.x + this.w >= F.totalWidth || nearToRock(this)) {
                        this.x -= this._speed;
                    }
                    if (!this.isPlaying("walk_right")) {
                        this.animate("walk_right");
                    }
                    wasLast = 'right';
                } else if (this._up) {
                    if (wasLast == 'right') {
                        this.sprite(3, 0, 1, 1);
                    } else if (wasLast == 'left') {
                        this.sprite(4, 0, 1, 1);
                    }
                } else if (wasLast) {
                    if (wasLast == 'right') {
                        this.sprite(0, 0, 1, 1);
                    } else if (wasLast == 'left') {
                        this.sprite(7, 0, 1, 1);
                    }
                    wasLast = false;
                }
            });
    },
    createStars: function() {
        F.inventory.stars = 0;
        for (var i = 0; i < 70; i++) {
            var x = 500 + ~~((Math.random() * 2 - 1) * 500);
            var y = 100 + ~~((Math.random() * 2 - 1) * 200);

            Crafty.e('2D, Canvas, Image, Star')
                .attr({
                    x: x,
                    y: y
                })
                .image('star.png');
        }
    },
    textField: function() {
        return Crafty.e('2D, DOM, Text, Dialog, Controls')
            .attr({
                w: F.screenWidth,
                h: 70,
                x: -Crafty.viewport.x,
                y: 442,
                z: 5
            })
            .textFont({
                size: "20px",
                family: "Silkscreen"
            })
            .css({
                background: "rgba(245, 245, 245, 0.95)",
                color: "black",
                padding: "5px"
            });
    },
    showNote: function(text, name, fn, x) {
        var inside;
        F.player.onHit(name, function(e) {
            if (inside || F.disabledNotes[name]) return;
            inside = true;
            Crafty('note').destroy();
            F.textField()
                .attr({
                    h: 50,
                    z: 5
                })
                .css({
                    "padding": "15px 5px 5px",
                    "text-align": "center"
                })
                .text(text)
                .addComponent('note')
                .bind('KeyDown', function(e) {
                    if (e.keyCode === Crafty.keys.ENTER) {
                        fn.call(this);
                        Crafty('note').destroy();
                    }
                }).bind('EnterFrame', function() {
                    this.x = -Crafty.viewport.x;
                });
        }, function() {
            inside = false;
            Crafty('note').destroy();
        });
    },
    disablePlayer: function() {
        F.player.disableControls = true;
        F.player.resetAnimation().pauseAnimation();
    },
    mouseWalk: function() {
        var j = 0,
            speed = 2,
            string;

        if (F.inventory.string) string = '2';
        else string = '';

        F.mouse._enterframeWalk = function() {
            if (j == 0 || j == 200) {
                speed = 2;
                this.animate("mouseRight" + string, -1);
                j = 0;
            }
            if (j == 100) {
                speed = -2;
                this.animate("mouseLeft" + string, -1);
            }
            this.x += speed;
            j++;
        };
        F.mouse.bind('EnterFrame', F.mouse._enterframeWalk);
    },
    showMouse: function() {
        if (Forest.mouseTriggered) return;
        Forest.mouseTriggered = true;

        F.mouse = Crafty.e("2D, Canvas, mouseSprite, SpriteAnimation, rat, Gravity, Collision")
            .attr({
                x: 1100 + F.player.x,
                y: 300
            })
            .reel("mouseLeft", 900, 0, 0, 2)
            .reel("mouseLeft2", 900, 2, 0, 2)
            .reel("mouseRight2", 900, 4, 0, 2)
            .reel("mouseRight", 900, 6, 0, 2)
            .animate("mouseLeft", -1)

        .bind("EnterFrame", function() {
            if (!Forest.mouseMet) this.x -= 2;
        })
            .collision()
            .onHit('Player', function() {
                if (F.mouseMet) return;
                F.mouseMet = true;
                this.pauseAnimation();

                //TODO
                //F.inventory.cheese=true;

                //if (!F.debug) {
                Forest.dialogMouse();
                //} else {
                //F.mouseWalk();
                //}
            });
    },
    dialogMouse: function() {
        var dialog = F.createDialog();
        dialog.addQuestion(1, "You new around here?", [
            ["Nah, I been rolling this hood since my birth", "2"],
            ["I guess so, yeah", "3"]
        ]);
        dialog.addQuestion(2, "Never seen you before...", [
            ["This city is a fucking jungle", "3"],
            ["Uh...", "3"]
        ]);
        dialog.addQuestion(3, "Nevermind, it ain't my bussines", [
            ["Shouldn't you be afraid of me, I'm a Dangerous Fox", "4"]
        ]);
        dialog.addQuestion(4, "Nah, you ain't a real fox. Everybody knows..", [
            ["...", "EXIT"],
            ["Uh...", "EXIT"]
        ]);

        dialog.playDialog(1);
        dialog.bind('Remove', function() {
            F.mouseWalk();
            setTimeout(function() {
                F.showNote('Talk to mouse', "rat", function() {
                    F.dialogMouse2();
                });
            }, 2000);
        });
    },
    dialogMouse2: function() {
        F.mouse.unbind('EnterFrame', F.mouse._enterframeWalk);
        var dialog = F.createDialog();

        if (F.rabbitHasFood) {
            dialog.addQuestion(1, "Have some string!", [
                ["How come?", "2"],
                ["Ok...", "EXIT"]
            ]);
            dialog.addQuestion(1, "Don't ask!", [
                ["K...", "EXIT"]
            ]);
            //dialog.addQuestion(1, "Could you carry me to the river? My feet hurt from walking in circles!", [["Do it yourself, man","EXIT"], ["Ok...","EXIT"]]);
            dialog.bind('Remove', function() {
                //if (this.dialogResults[1]) {
                F.inventory.string = true;
                F.disabledNotes.bear = false;
                //}
                //TODO
                //Jump on fox
                //F.mouseWalk();
            });
        } else if (F.inventory.cheese) {
            dialog.addQuestion(1, "Hey", [
                ["You interested in some tasty cheese?", "2"],
                ["Hi (Leave)", "EXIT"]
            ]);
            dialog.addQuestion(2, "I love cheese", [
                ["How about trading it against that string", "3"]
            ]);
            dialog.addQuestion(3, "Fine with me", [
                ["Cool", "EXIT"]
            ]);
            dialog.bind('Remove', function() {
                if (typeof this.dialogResults[2] == 'number' && this.dialogResults[2] == 0) {
                    F.dropCarrot();
                    F.disabledNotes.bear = false;
                    F.inventory.string = true;
                    F.inventory.cheese = false;
                }
                F.mouseWalk();
            });
        } else {
            dialog.addQuestion(1, "Hey", [
                ["Hi", "EXIT"]
            ]);
            dialog.bind('Remove', function() {
                F.mouseWalk();
            });
        }
        dialog.playDialog(1);
    },
    dialogCrow: function() {
        F.disabledNotes.rat = true;
        F.player.x = 2890;
        F.crow.pauseAnimation();
        //F.crow.stop();
        if (typeof F.stolenCheese == 'undefined') {
            var dialog = F.createDialog();
            dialog.addQuestion(1, "", [
                ["'Sup, dude?", "2"],
                ["How's it goin?", "2"]
            ]);
            dialog.addQuestion(2, "...", [
                ["Your not of the talkative kind, are you?", "3"],
                ["Really nice piece of cheese you got there", "3b"]
            ]);
            dialog.addQuestion(3, "...", [
                ["Too proud to talk to me? Fuck off.", "EXIT"],
                ["I guess not. Anyway, you look mighty fine today, my Dear Sir.", "4"]
            ]);
            dialog.addQuestion('3b', "...", [
                ["Anyway, you look mighty fine today, my Dear Sir.", "4"],
                ["(Ignore)", "EXIT"]
            ]);
            dialog.addQuestion(4, "...", [
                ["Your feathers seem so glossy, you recently switched your shampoo brand?", "5"],
                ["(This is getting ridicoulous...)", "EXIT"]
            ]);
            dialog.addQuestion(5, "...", [
                ["(Leave...)", "EXIT"],
                ["If you could sing as good as you look, you'd be like .. dunno .. the king of all birds.", "6", F.dropCheese]
            ]);
            dialog.addQuestion(6, "(Tries to sing)", [
                ["(Take cheese)", "EXIT"],
                ["You seem to have lost something", "7", F.flyDown]
            ]);
            dialog.addQuestion(7, "Uh..thanks", [
                ["Dontcha worry, bro, I got you covered", "EXIT"]
            ]);

            dialog.bind('Remove', function() {
                if (typeof this.dialogResults[6] == 'undefined') return;
                if (this.dialogResults[6] == 0) {
                    F.stolenCheese = true;
                    F.inventory.cheese = true;
                    F.cheese.destroy();
                } else if (this.dialogResults[6] == 1) {
                    F.stolenCheese = false;
                    F.inventory.cheese = false;
                }
                try {
                    F.bubble.destroy();
                } catch (e) {}
            });
        } else if (F.stolenCheese == true) {
            var dialog = F.createDialog();
            dialog.addQuestion(1, "You stole my cheese, I don't want to talk to you", [
                ["(Leave)", "EXIT"]
            ]);
        } else if (F.stolenCheese == false && F.inventory.crackers) {
            var dialog = F.createDialog();
            dialog.addQuestion(1, "", [
                ["How about, I give you some crackers and get your cheese in return", "2"],
                ["(Leave)", "EXIT"]
            ]);
            dialog.addQuestion(2, "Sure, I don't really like this cheese anyway", [
                ["(Trade)", "EXIT"]
            ]);
            dialog.bind('Remove', function() {
                if (typeof this.dialogResults[2] != 'undefined') {
                    F.inventory.cheese = true;
                    F.inventory.crackers = false;
                    F.crow.animate("crowFlying", 150, -1);
                }
            });
        } else {
            var dialog = F.createDialog();
            dialog.addQuestion(1, "Hey", [
                ["Hiya", "EXIT"],
                ["Yo", "EXIT"]
            ]);
        }
        dialog.playDialog(1);
        dialog.bind('Remove', function() {
            F.disabledNotes.rat = false;
        });

    },
    dialogMushroom: function() {
        var dialog = F.createDialog();

        dialog.addQuestion(1, "", [
            ["Keep it", "EXIT"],
            ["Eat now", "EXIT", F.die]
        ]);
        dialog.addQuestion(2, "...", [
            ["", "3"],
            ["", "3b"]
        ]);

        dialog.playDialog(1);
        dialog.bind('Remove', function() {
            F.mushroom.destroy();
            F.inventory.mushroom = true;
        });
    },
    dialogRabbit: function() {
        var dialog = F.createDialog();

        if (!F.talkedToRabbit) {
            dialog.addQuestion(1, "", [
                ["How come you're not playing that accordeon?", "2"]
            ]);
            dialog.addQuestion(2, "I'm too hungry, but I can't leave or someone might take my instrument.", [
                ["Why don't you take it with you?", "3"],
                ["That's unfortunate (Leave)", "EXIT"]
            ]);
            dialog.addQuestion(3, "I'm too hungry to carry it either", [
                ["That's just fucked up. (Leave)", "EXIT"],
                ["I'll have a look if I find some food.", "EXIT"]
            ]);
            dialog.bind('Remove', function() {
                F.talkedToRabbit = true;
            });
        } else {
            var food;
            if (F.inventory.crackers) food = 'crackers';
            if (F.inventory.carrot) food = 'carrot';
            if (food) {
                dialog.addQuestion(1, "Can i haz food?", [
                    ["Yep, some " + food, "2"],
                    ["Nah, sorry", "EXIT"]
                ]);
                dialog.addQuestion(2, "(om nom nom..)", [
                    ["(Leave)", "EXIT"]
                ]);
                dialog.bind('Remove', function() {
                    F.rabbitHasFood = true;
                    F.rabbit.animate("rabbitPlaying", 40, -1);
                    F.animalsPlaying++;
                    if (F.animalsPlaying == 2) F.fadeAndPlay();
                    //TODO mouse needs to come and contact
                    //F.mouse
                });
            } else if (F.inventory.cheese) {
                dialog.addQuestion(1, "Can i haz food?", [
                    ["Yep, some cheese", "2"],
                    ["Nah, sorry", "EXIT"]
                ]);
                dialog.addQuestion(2, "I hate cheese!", [
                    ["(Leave)", "EXIT"]
                ]);
            } else {
                dialog.addQuestion(1, "Did you find some food?", [
                    ["Nah, sorry", "EXIT"]
                ]);
            }
        }
        dialog.playDialog(1);
    },
    dialogBear: function() {
        if (!F.talkedToBear) {
            var dialog = F.createDialog();
            dialog.addQuestion(1, "", [
                ["How come you're not playing that banjo?", "2"]
            ]);
            dialog.addQuestion(2, "I wish I could, but one of the strings broke.", [
                ["That ain't good", "EXIT"],
                ["Uh..", "EXIT"]
            ]);
            dialog.bind('Remove', function() {
                F.talkedToBear = true;
                if (!F.inventory.string) F.disabledNotes.bear = true;
            });
            dialog.playDialog(1);
        } else if (F.inventory.string) {
            var dialog = F.createDialog();
            dialog.addQuestion(1, "", [
                ["Look, I found a replacement string", "2"]
            ]);
            dialog.addQuestion(2, "Thanks!", [
                ["...", "EXIT"]
            ]);
            dialog.playDialog(1);
            dialog.bind('Remove', function() {
                F.bear.animate("bearPlaying", 40, -1);
                F.animalsPlaying++;
                if (F.animalsPlaying == 2) F.fadeAndPlay();
            });

        }
    },
    dialogSquirrel: function() {
        if (typeof F.stolenCheese != 'undefined' && F.stolenCheese == false && !F.inventory.crackers) {
            var dialog = F.createDialog();
            dialog.addQuestion(1, "I heard you didn't steal the crow's cheese, even though you could.", [
                ["So?", "2"],
                ["Yep", "2"]
            ]);
            dialog.addQuestion(2, "Here, have some crackers, I don't really like them anyway", [
                ["kthx!", "EXIT"]
            ]);
            dialog.playDialog(1);
            F.inventory.crackers = true;
        } else if (!F.talkedToSquirrel) {
            var dialog = F.createDialog();
            dialog.addQuestion(1, "Hey", [
                ["What're you doing here", "2"],
                ["'Sup?", "2"]
            ]);
            dialog.addQuestion(2, "I'm just waiting", [
                ["For what?", "3"]
            ]);
            dialog.addQuestion(3, "I dunno yet", [
                ["Sure..", "4"],
                ["Ok", "4"]
            ]);
            dialog.addQuestion(4, "Anyway, I'll show you something if you bring me some stars", [
                ["(Leave)", "EXIT"]
            ]);
            dialog.bind('Remove', function() {
                F.talkedToSquirrel = true;
                Crafty('Star').each(function() {
                    this.addComponent("Collision").onHit('Player', function() {
                        this.destroy();
                        F.inventory.stars++;
                    });
                });

            });
            dialog.playDialog(1);
        } else if (F.talkedToSquirrel) {
            if (F.inventory.stars) {
                var dialog = F.createDialog();
                dialog.addQuestion(1, "", [
                    ["I got you some stars", "2"]
                ]);
                dialog.addQuestion(2, "Cool! Ok, now wait a second", [
                    ["Sure", "EXIT"]
                ]);
                dialog.playDialog(1);
                dialog.bind('Remove', function() {
                    F.wakeUp();
                });
            }
        }
    },
    createDialog: function() {
        F.disablePlayer();
        var dialog = F.textField().dialog();
        dialog.bind('Remove', function() {
            F.player.disableControls = false;
        });
        return dialog;
    },
    dropCheese: function() {
        //Stop animation
        F.crow.unbind("EnterFrame", this.drawFrame);
        F.cheese = Crafty.e("2D, Canvas, Image, Gravity")
            .attr({
                w: 24,
                h: 24,
                x: 3060,
                y: 100
            })
            .image('cheese.png')
            .gravity("floor");
        F.crow.sprite(0, 0, 1, 1);
        F.bubble = Crafty.e("2D, Canvas, Image")
            .attr({
                x: 3030,
                y: 25
            })
            .image('sprechblase.png');
    },
    dropCarrot: function() {
        F.carrot = Crafty.e("2D, Canvas, Image, carrot")
            .attr({
                x: 810,
                y: 380
            })
            .image('carrot.png');
        F.showNote('Pick up carrot', "carrot", function() {
            F.carrot.destroy();
            F.inventory.carrot = true;
        }, 610);
    },
    flyDown: function() {
        F.bubble.destroy();
        F.crow.z = 3;
        F.crow.animate('crowFlying', 10, -1).addComponent('gravity').gravity('floor').bind('Hit', function() {
            this.unbind('Hit');
            this.unbind('EnterFrame');
            F.cheese.destroy();
            this.animate('crowWithCheese', 150, -1);
            //this.stop().animate('crowWithCheese', 150, -1);

        });
    },
    fadeAndPlay: function() {
        Crafty.audio.mute();
        Crafty.unbind('Pause');
        Crafty.unbind('Unpause');
        Crafty.audio.play("music");
        var opacity = 0;
        F.player.disableControls = true;
        Crafty.viewport.x = -4134;

        var solid = Crafty.e("2D, Canvas, Color").attr({
            w: F.screenWidth,
            h: F.screenHeight,
            x: -Crafty.viewport.x,
            y: 0,
            z: 50
        }).color("#000000");

        solid.alpha = 0;

        F.player.bind('EnterFrame', function() {
            opacity += 0.0025;
            solid.alpha = opacity.toFixed(2);
            if (opacity > 0.7) {
                F.player.unbind('EnterFrame');
                var dialog = F.textField()
                    .attr({
                        w: F.screenWidth,
                        h: F.screenHeight,
                        x: -Crafty.viewport.x,
                        y: 0,
                        z: 100
                    })
                    .css({
                        background: "none",
                        "text-align": "center",
                        margin: "0px",
                        color: 'white'
                    });
                dialog.text("<h2>And so, they played happily thereafter</h2><br><br><br><br><br><br><br><br><br><br><h3>Reload to start again</h3><h6>Music by <a href='http://soundcloud.com/pbailey/' target=_blank>Paul Bailey Esemble</a></h6>");
            }
        });

    },
    die: function() {
        var opacity = 1;
        F.player.bind('EnterFrame', function() {
            opacity -= 0.0035;

            this.alpha = (opacity + (Math.random() * 2 - 1) / 2).toFixed(2);
            if (opacity < -0.05) {
                F.player.disableControls = true;
                F.player.unbind('EnterFrame');
                F.textField()
                    .attr({
                        w: F.screenWidth,
                        h: F.screenHeight,
                        x: -Crafty.viewport.x,
                        y: 0,
                        z: 10
                    })
                    .css({
                        background: "rgba(250, 250, 250, 0.8)",
                        "text-align": "center",
                        margin: "0px"
                    })
                    .text('<h1>Uh, that hurt!</h1><h3>Reload to start again</h3>');
            }
        });
    },
    wakeUp: function() {
        var opacity = 0;
        F.player.disableControls = true;
        var solid = Crafty.e("2D, Canvas, Color").attr({
            w: F.screenWidth,
            h: F.screenHeight,
            x: -Crafty.viewport.x,
            y: 0,
            z: 50
        }).color("#ffffff");

        solid.alpha = 0;

        F.player.unbind('EnterFrame');
        try {
            Crafty('note').destroy();
        } catch (e) {}
        Crafty.bind('EnterFrame', function() {
            opacity += 0.0035;

            solid.alpha = (opacity + (Math.random() * 2 - 1) / 2).toFixed(2);
            if (opacity > 1.05) {
                solid.alpha = 1;

                F.player.unbind('EnterFrame');
                var dialog = F.textField()
                    .attr({
                        w: F.screenWidth,
                        h: F.screenHeight,
                        x: -Crafty.viewport.x,
                        y: 0,
                        z: 10
                    })
                    .css({
                        background: "rgba(0, 0, 0, 1)",
                        "text-align": "center",
                        margin: "0px",
                        color: 'white'
                    });
                dialog.addComponent('Image').image('wake.png').text('<h2>This was a weird dream. Seriously!</h2><br><br><br><br><br><br><br><br><br><br><br><br><h3>Reload to start again</h3>');
            }
        });
    }
};

//Additional components
Crafty.c("Dialog", {
    init: function() {
        if (!this.has("Keyboard")) this.addComponent("Keyboard");
    },
    dialog: function() {
        this.dialogTexts = {};
        this.dialogResults = {};
        return this;
    },
    addQuestion: function(id, question, answers) {
        this.dialogTexts[id] = [question, answers];
        return this;
    },
    playDialog: function(id) {
        var q = this.dialogTexts[id][0];
        var a = this.dialogTexts[id][1];

        var selected = 0;
        var count = a.length;
        this.setDialogText(q, a, selected);

        var move = this.__move = {
            up: false,
            down: false,
            enter: false
        };
        var wait = false;
        this.bind("EnterFrame", function() {
            if (wait) return;
            if (this.isDown("DOWN_ARROW") || this.isDown("S")) {
                selected++;
                if (selected == count) selected = 0;
                this.setDialogText(q, a, selected);
                wait = true;
            }
            if (this.isDown("ENTER")) {
                wait = true;
                this.dialogResults[id] = selected;

                id = a[selected][1];
                //Execute callback
                if (typeof a[selected][2] == 'function') a[selected][2].call();
                if (id == "EXIT") this.destroy();
                else {
                    //We're changing questions. Set vars to new content
                    q = this.dialogTexts[id][0];
                    a = this.dialogTexts[id][1];
                    count = a.length;
                    selected = 0;
                    this.setDialogText(q, a, selected);
                }

            }
            if (this.isDown("UP_ARROW") || this.isDown("W")) {
                selected--;
                if (selected == -1) selected = count - 1;
                this.setDialogText(q, a, selected);
                wait = true;
            }
        }).bind("KeyUp", function() {
            wait = false;
        });
        return this;
    },
    setDialogText: function(q, a, selected) {
        var text = "";
        if (q !== "") {
            text = q + "<br>";
        }
        text += "<span class='answers'>";
        for (var i = 0; i < a.length; i++) {
            text += "<span>";
            if (i == selected) text += "> ";
            text += a[i][0] + "</span><br>";
        }
        text += "</span>";
        this.text(text);
    }
});

Crafty.c("BackgroundCanvas", {
    images: [],
    init: function() {
        if (!Crafty._backgroundCanvas) {
            var c;

            c = document.createElement("canvas");
            c.width = Crafty.viewport.width;
            c.height = Crafty.viewport.height;
            c.style.position = 'absolute';
            c.style.left = "0px";
            c.style.top = "0px";
            c.style.zIndex = "-50";
            Crafty.stage.elem.appendChild(c);

            Crafty._backgroundCanvas = c;
        }
        var ctx = Crafty._backgroundCanvas.getContext('2d');
        this.ctx = ctx;

        var oldViewport = {
            x: Crafty.viewport.x,
            y: Crafty.viewport.y
        };
        this.viewportDelta = {};
        this.needsDraw = false;

        this.bind('EnterFrame', function() {
            this.viewportDelta = {
                x: Crafty.viewport.x - oldViewport.x,
                y: Crafty.viewport.y - oldViewport.y
            };
            oldViewport = {
                x: Crafty.viewport.x,
                y: Crafty.viewport.y
            };
            if (!this.viewportDelta.x && !this.viewportDelta.y && !this.needsDraw) {
                return;
            }


            //Draw visible
            var startOffset = Math.floor(-Crafty.viewport.x / 800);
            var number = Math.ceil(Crafty.viewport.width / 800);
            var stopOffset = startOffset + number;
            //console.log(imagesToDraw);

            for (var i = startOffset; i < stopOffset; i++) {
                var img = this.images[i];
                //skip if no image
                if (!img.ready || !img._pattern) return;

                ctx.fillStyle = img._pattern;

                ctx.save();
                ctx.translate(Crafty.viewport.x + (i * img.w), 0);
                ctx.fillRect(0, 0, img.w, img.h);
                ctx.restore();
            }
            this.needsDraw = false;
        });
        return this;

    },
    addImage: function(url) {
        var image = {};
        image.asset = Crafty.asset(url);

        if (!image.asset) {
            image.asset = new Image();
            Crafty.asset(url, image.asset);
            image.asset.src = url;
            var self = image;

            image.asset.onload = function() {
                self._pattern = this.ctx.createPattern(self.img, "no-repeat");
                self.ready = true;

                self.w = self.img.width;
                self.h = self.img.height;
            };
        } else {
            image.ready = true;
            image._pattern = this.ctx.createPattern(image.asset, "no-repeat");
            image.w = image.asset.width;
            image.h = image.asset.height;
        }

        this.images.push(image);
        this.needsDraw = true;
        return this;
    }
});

var F = Forest;
F.debug = true;
if (window.location.href.indexOf('leo-koppelkamm.de') != -1) F.debug = false;

//Crafty.deactivateParticles = true;

if (navigator.userAgent.indexOf('iOS') != -1) Crafty.deactivateParticles = true;
// if (F.debug) {
//     F.speed = 26;
//     Crafty.dontPauseOnBlur = true;
//     F.jump = 10;
// }
window.onload = Forest.init;